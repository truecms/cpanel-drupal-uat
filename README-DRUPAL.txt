If you run Drupal website, you may benefit from setting up Dev and UAT
enfironments as outlined in this article:
https://redy.host/knowledgebase/cpanel/drupal/create-and-use-dev-and-test-environment-cpanel

To get started, visit the link above and follow steps of configuring your Drupal
installation. We have created environment configuration folders for you:
 - configuration/
 and configuration override files already placed there. You need to update them
 to have correct database credentials and domain name. Check the configuration/
 folder for dev-settings.env.inc and uat-settings.env.inc files.
