<?php
/**
 * @file
 * default.settings.php
 *
 * Place the content of this file (excluding this comment) at the bottom of
 * your settings.php file in Drupal. This allows for environmental overrides
 * for Dev and UAT environments.
 */

// Disable Shield module authentication for production.
// The environmental include files will override this setting.
$conf['shield_user'] = '';
$conf['shield_pass'] = '';

// Identify environment based on first 3 characters of the subdomain.
$environment = substr($_SERVER['SERVER_NAME'], 0, 3);
if (file_exists(dirname(__FILE__) . '/../../../configuration/' . $environment . '-settings.env.inc')) {
  include_once dirname(__FILE__) . '/../../../configuration/' . $environment . '-settings.env.inc';
}
