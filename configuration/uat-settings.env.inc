<?php
/**
 * @file
 * UAT/Dev environment configuration file override for Drupal 7 or Drupal 8.
 *
 * In order to use this file, add small line of code to your Drupal's
 * settings.php file as outlined here:
 * https://redy.host/knowledgebase/cpanel/drupal/create-and-use-dev-and-test-environment-cpanel#settings.php
 * or displayed below.
 *
 * // Disable Shield module authentication for production.
 * // The environmental include files will override this setting.
 * $conf['shield_user'] = '';
 * $conf['shield_pass'] = '';
 *
 * // Identify environment based on first 3 characters of the subdomain
 * $environment = substr($_SERVER['SERVER_NAME'], 0, 3);
 * if (file_exists(dirname(__FILE__) . '/../../../configuration/' . $environment . '-settings.env.inc')) {
 * include_once dirname(__FILE__) . '/../../../configuration/' . $environment . '-settings.env.inc';
 * }
 *
 * This functionality is dependant on subdomains that have to be created in
 * advance.
 *
 * Read inline comments for more information.
 */
$databases = array(
  'default' =>
    array(
      'default' =>
        array(
          'database' => '<database>',
          'username' => '<username>',
          'password' => '<password>',
          'host' => 'localhost',
          'port' => '3069',
          'driver' => 'mysql',
          'prefix' => '',
        ),
    ),
);

$domain = '<yoursite.com>';
$env = 'uat';
$protocol = 'http://';

$cookie_domain = $env . '.' . $domain;
$base_url = $protocol . $cookie_domain;

// Unset any cache backends (such as memcached or redis).
unset($conf['cache_backends']);
unset($conf['cache_default_class']);
unset($conf['cache_class_cache_form']);

$conf['file_private_path'] = 'sites/default/files';
$conf['file_temporary_path'] = '/tmp';

// Tell stage_file_proxy module to load file uploads from Production
// site.
$conf['stage_file_proxy_origin'] = $protocol . $domain . '/';

ini_set('display_errors', 1);
// Protect your dev/uat environment with username/password
// to prevent anyone looking at them and to keep search engines
// away.
// Shield module (must be installed) username/password:
$conf['shield_user'] = 'simpleuser';
$conf['shield_pass'] = 'simplepassword';
